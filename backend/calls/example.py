# -*- coding: utf-8 -*-

# Sample Python code for youtube.channels.list
# See instructions for running these code samples locally:
# https://developers.google.com/explorer-help/guides/code_samples#python

import os

import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors
import pickle
import json
from google.oauth2.credentials import Credentials


scopes = ["https://www.googleapis.com/auth/youtube.readonly"]


def main(token):
    print("hello")
    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    api_service_name = "youtube"
    api_version = "v3"
    with open('YOUR_CLIENT_SECRET_FILE.json') as f:
        client_secrets_file = json.load(f)

    # Get credentials and create an API client
    credentials = Credentials(
        token,
        refresh_token="SAVED REFRESH TOKEN",
        token_uri=client_secrets_file['web']['token_uri'],
        client_id=client_secrets_file['web']['client_id'],
        client_secret=client_secrets_file['web']['client_secret']
    )
    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, credentials=credentials)

    request = youtube.channels().list(
        part="snippet,contentDetails,statistics",
        id="UC_x5XG1OV2P6uZZ5FSM9Ttw"
    )
    response = request.execute()

    return response
