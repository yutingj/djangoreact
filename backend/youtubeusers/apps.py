from django.apps import AppConfig


class YoutubeusersConfig(AppConfig):
    name = 'youtubeusers'
