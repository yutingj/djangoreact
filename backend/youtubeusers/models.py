from djongo import models


class Youtubeuser(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    email = models.CharField(max_length=100, blank=True, default='')
    password = models.TextField()
