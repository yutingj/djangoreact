from rest_framework import serializers
from youtubeusers.models import Youtubeuser


class YoutubeuserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Youtubeuser
        fields = ['id', 'email', 'password']
