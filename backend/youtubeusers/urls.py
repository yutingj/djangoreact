from django.urls import path
from youtubeusers import views

urlpatterns = [
    path('youtubeusers/', views.youtubeuser_list),
    path('youtubeusers/<int:pk>/', views.youtubeuser_detail),
]
