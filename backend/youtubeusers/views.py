from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from youtubeusers.models import Youtubeuser
from youtubeusers.serializers import YoutubeuserSerializer
from calls.example import main
import urllib.parse as urlparse
from urllib.parse import parse_qs


@api_view(['GET', 'POST'])
def youtubeuser_list(request):
    """
    List all code youtubeusers, or create a new youtubeuser.
    """
    print(request.GET.get('access_token'))
    if request.method == 'GET':
        youtubeusers = Youtubeuser.objects.all()
        serializer = YoutubeuserSerializer(youtubeusers, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = YoutubeuserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def youtubeuser_detail(request, pk):
    """
    Retrieve, update or delete a code youtubeuser.
    """
    try:
        youtubeuser = Youtubeuser.objects.get(pk=pk)
    except Youtubeuser.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = YoutubeuserSerializer(youtubeuser)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = YoutubeuserSerializer(youtubeuser, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        youtubeuser.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def channel_view(request):
    """
    Retrieve a channel
    """
    print(request.GET.get('access_token'))
    try:
        response = main(request.GET.get('access_token'))
    except Youtubeuser.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        return Response(response)
