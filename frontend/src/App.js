// frontend/src/App.js

import React, { Component } from "react";
import axios from "axios";
import GoogleLogin from "./components/GoogleLogin";

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			viewCompleted: false,
			activeItem: {
				title: "",
				description: "",
				completed: false,
			},
			todoList: [],
		};
	}

	componentDidMount() {}

	// refreshList = () => {
	// 	axios
	// 		.get("http://localhost:8000/api/currentuser/")
	// 		.then((res) => this.setState({ todoList: res.data }))
	// 		.catch((err) => console.log(err));
	// };

	render() {
		return (
			<main className="content">
				<div className="App">
					<GoogleLogin />
				</div>
			</main>
		);
	}
}
export default App;
