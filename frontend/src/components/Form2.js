import React from "react";
import axios from "axios";

export default class Form2 extends React.Component {
	constructor(props) {
		super(props);
		this.state = { email: "", password: "" };

		this.handleChangeEmail = this.handleChangeEmail.bind(this);
		this.handleChangePassword = this.handleChangePassword.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChangeEmail(event) {
		this.setState({ email: event.target.value });
	}

	handleChangePassword(event) {
		this.setState({ password: event.target.value });
	}

	handleSubmit(event) {
		axios
			.get("http://localhost:8000/api/youtubechannel")
			.then((res) => alert(res));

		event.preventDefault();
	}

	render() {
		return (
			<form onSubmit={this.handleSubmit}>
				<label>
					Email:
					<input
						type="text"
						value={this.state.value}
						onChange={this.handleChangeEmail}
					/>
				</label>
				<label>
					Password:
					<input
						type="password"
						value={this.state.value}
						onChange={this.handleChangePassword}
					/>
				</label>
				<input type="submit" value="Submit" />
			</form>
		);
	}
}
